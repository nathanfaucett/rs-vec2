vec2
=====

rust vector 2 functions

```rust
extern crate vec2;

fn main() {
    let a = [1, 1];
    let b = [1, 1];
    let mut out = vec2::new(0, 0);

    vec2::add(&mut out, &a, &b);

    assert_eq!(out, [2, 2]);
}
```
