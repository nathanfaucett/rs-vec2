use number_traits::{Cast, Num, Signed};

#[inline]
pub fn inverse<'a, T: Copy + Signed>(out: &'a mut [T; 2], a: &[T; 2]) -> &'a mut [T; 2] {
    out[0] = -a[0];
    out[1] = -a[1];
    out
}
#[test]
fn test_inverse() {
    let mut v = [0, 0];
    inverse(&mut v, &[1, 1]);
    assert!(v[0] == -1);
    assert!(v[1] == -1);
}

#[inline]
pub fn lerp<'a, T, F>(out: &'a mut [T; 2], a: &[T; 2], b: &[T; 2], t: F) -> &'a mut [T; 2]
where
    T: Copy + Num + Cast<F>,
    F: Copy + Num + Cast<T>,
{
    let a0 = a[0].cast();
    let a1 = a[1].cast();
    let b0 = b[0].cast();
    let b1 = b[1].cast();

    out[0] = (a0 + (b0 - a0) * t).cast();
    out[1] = (a1 + (b1 - a1) * t).cast();
    out
}
#[test]
fn test_lerp() {
    let mut v = [0, 0];
    lerp(&mut v, &[0, 0], &[2, 4], 0.5);
    assert_eq!(v[0], 1);
    assert_eq!(v[1], 2);
}

#[inline]
pub fn perp_l<'a, T: Copy + Signed>(out: &'a mut [T; 2], a: &[T; 2]) -> &'a mut [T; 2] {
    out[0] = a[1];
    out[1] = -a[0];
    out
}
#[test]
fn test_perp_l() {
    let mut v = [0, 0];
    perp_l(&mut v, &[1, 1]);
    assert!(v[0] == 1);
    assert!(v[1] == -1);
}

#[inline]
pub fn perp_r<'a, T: Copy + Signed>(out: &'a mut [T; 2], a: &[T; 2]) -> &'a mut [T; 2] {
    out[0] = -a[1];
    out[1] = a[0];
    out
}
#[test]
fn test_perp_r() {
    let mut v = [0, 0];
    perp_r(&mut v, &[1, 1]);
    assert!(v[0] == -1);
    assert!(v[1] == 1);
}

#[inline]
pub fn min<'a, T: Copy + Num>(out: &'a mut [T; 2], a: &[T; 2], b: &[T; 2]) -> &'a mut [T; 2] {
    out[0] = if b[0] < a[0] { b[0] } else { a[0] };
    out[1] = if b[1] < a[1] { b[1] } else { a[1] };
    out
}
#[test]
fn test_min() {
    let mut v = [0, 0];
    min(&mut v, &[1, 0], &[0, 1]);
    assert!(v == [0, 0]);
}

#[inline]
pub fn max<'a, T: Copy + Num>(out: &'a mut [T; 2], a: &[T; 2], b: &[T; 2]) -> &'a mut [T; 2] {
    out[0] = if b[0] > a[0] { b[0] } else { a[0] };
    out[1] = if b[1] > a[1] { b[1] } else { a[1] };
    out
}
#[test]
fn test_max() {
    let mut v = [0, 0];
    max(&mut v, &[1, 0], &[0, 1]);
    assert!(v == [1, 1]);
}

#[inline]
pub fn clamp<'a, T: Copy + Num>(
    out: &'a mut [T; 2],
    a: &[T; 2],
    min: &[T; 2],
    max: &[T; 2],
) -> &'a mut [T; 2] {
    out[0] = if a[0] < min[0] {
        min[0]
    } else if a[0] > max[0] {
        max[0]
    } else {
        a[0]
    };
    out[1] = if a[1] < min[1] {
        min[1]
    } else if a[1] > max[1] {
        max[1]
    } else {
        a[1]
    };
    out
}
#[test]
fn test_clamp() {
    let mut v = [0, 0];
    clamp(&mut v, &[2, 2], &[0, 0], &[1, 1]);
    assert!(v == [1, 1]);
}

#[inline]
pub fn eq<T: Copy + Num>(a: &[T; 2], b: &[T; 2]) -> bool {
    !ne(a, b)
}
#[test]
fn test_eq() {
    assert_eq!(eq(&[1f32, 1f32], &[1f32, 1f32]), true);
    assert_eq!(eq(&[0f32, 0f32], &[1f32, 1f32]), false);
}

#[inline]
pub fn ne<T: Copy + Num>(a: &[T; 2], b: &[T; 2]) -> bool {
    !a[0].approx_eq(&b[0]) || !a[1].approx_eq(&b[1])
}
#[test]
fn test_ne() {
    assert_eq!(ne(&[1f32, 1f32], &[1f32, 1f32]), false);
    assert_eq!(ne(&[0f32, 0f32], &[1f32, 1f32]), true);
}
